-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 13, 2018 at 08:45 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `HW1`
--

-- --------------------------------------------------------

--
-- Table structure for table `Customer`
--

CREATE TABLE `Customer` (
  `ID` varchar(10) NOT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `Password` varchar(10) DEFAULT NULL,
  `FirstName` varchar(10) DEFAULT NULL,
  `LastName` varchar(10) DEFAULT NULL,
  `Sex` varchar(5) DEFAULT NULL,
  `Credit` float DEFAULT NULL,
  `Cellphone` varchar(10) DEFAULT NULL,
  `Address` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Manager`
--

CREATE TABLE `Manager` (
  `ID` varchar(10) NOT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `Password` varchar(10) DEFAULT NULL,
  `FirstName` varchar(10) DEFAULT NULL,
  `LastName` varchar(10) DEFAULT NULL,
  `Sex` varchar(5) DEFAULT NULL,
  `Cellphone` varchar(10) DEFAULT NULL,
  `Address` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Order1`
--

CREATE TABLE `Order1` (
  `ID` varchar(10) NOT NULL,
  `VendorID` varchar(10) DEFAULT NULL,
  `CustomerID` varchar(10) DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL,
  `PaymentType` varchar(10) DEFAULT NULL,
  `CreatedAt` date DEFAULT NULL,
  `DeliveryAddress` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `OrderProduct`
--

CREATE TABLE `OrderProduct` (
  `OrderID` varchar(10) NOT NULL,
  `ProductID` varchar(10) NOT NULL,
  `Amount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Product`
--

CREATE TABLE `Product` (
  `ID` varchar(10) NOT NULL,
  `Title` varchar(10) DEFAULT NULL,
  `Price` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Vendor`
--

CREATE TABLE `Vendor` (
  `ID` varchar(10) NOT NULL,
  `Title` varchar(10) DEFAULT NULL,
  `City` varchar(10) DEFAULT NULL,
  `ManagerID` varchar(10) DEFAULT NULL,
  `Phone` varchar(10) DEFAULT NULL,
  `Address` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `VendorProduct`
--

CREATE TABLE `VendorProduct` (
  `VendorID` varchar(10) NOT NULL,
  `ProductID` varchar(10) NOT NULL,
  `Amount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Customer`
--
ALTER TABLE `Customer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Manager`
--
ALTER TABLE `Manager`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Order1`
--
ALTER TABLE `Order1`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CustomerID` (`CustomerID`),
  ADD KEY `VendorID` (`VendorID`);

--
-- Indexes for table `OrderProduct`
--
ALTER TABLE `OrderProduct`
  ADD PRIMARY KEY (`OrderID`,`ProductID`),
  ADD KEY `ProductID` (`ProductID`);

--
-- Indexes for table `Product`
--
ALTER TABLE `Product`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Vendor`
--
ALTER TABLE `Vendor`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ManagerID` (`ManagerID`);

--
-- Indexes for table `VendorProduct`
--
ALTER TABLE `VendorProduct`
  ADD PRIMARY KEY (`VendorID`,`ProductID`),
  ADD KEY `ProductID` (`ProductID`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Order1`
--
ALTER TABLE `Order1`
  ADD CONSTRAINT `Order1_ibfk_1` FOREIGN KEY (`CustomerID`) REFERENCES `Customer` (`ID`),
  ADD CONSTRAINT `Order1_ibfk_2` FOREIGN KEY (`VendorID`) REFERENCES `Vendor` (`ID`);

--
-- Constraints for table `OrderProduct`
--
ALTER TABLE `OrderProduct`
  ADD CONSTRAINT `OrderProduct_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `Order1` (`ID`),
  ADD CONSTRAINT `OrderProduct_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`);

--
-- Constraints for table `Vendor`
--
ALTER TABLE `Vendor`
  ADD CONSTRAINT `Vendor_ibfk_1` FOREIGN KEY (`ManagerID`) REFERENCES `Manager` (`ID`);

--
-- Constraints for table `VendorProduct`
--
ALTER TABLE `VendorProduct`
  ADD CONSTRAINT `VendorProduct_ibfk_1` FOREIGN KEY (`VendorID`) REFERENCES `Vendor` (`ID`),
  ADD CONSTRAINT `VendorProduct_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
