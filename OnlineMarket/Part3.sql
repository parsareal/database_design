-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 21, 2018 at 01:22 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `HW1`
--

-- --------------------------------------------------------

--
-- Table structure for table `Customer`
--

CREATE TABLE `Customer` (
  `ID` varchar(10) NOT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `Password` varchar(10) DEFAULT NULL,
  `FirstName` varchar(10) DEFAULT NULL,
  `LastName` varchar(10) DEFAULT NULL,
  `Sex` varchar(10) DEFAULT NULL,
  `Credit` float DEFAULT NULL,
  `Cellphone` varchar(10) DEFAULT NULL,
  `Address` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Customer`
--

INSERT INTO `Customer` (`ID`, `Email`, `Password`, `FirstName`, `LastName`, `Sex`, `Credit`, `Cellphone`, `Address`) VALUES
('c1', 'parsareal@gmail.com', 'somepass', 'parsa', 'kaveh', 'male', 300, '0919805442', 'Shariati'),
('c10', 'abedigram@gmail.com', '234839', 'bagher', 'abedi', 'male', 654, '2893574', 'Tehranpars'),
('c2', 'mhmd@gmail.com', 'somepass', 'mohammad', 'samadi', 'male', 100, '93924598', 'Ati saz'),
('c3', 'sarb98@gmail.com', '0130ofids', 'alireza', 'bakhtiari', 'male', 500, '32019540', 'Daneshgah square'),
('c4', 'samimd@gmail.com', 'qoewrfsj', 'mohammad', 'sami', 'male', 250, '3045250', 'Haft hoze square'),
('c5', 'dbabadi@gmail.com', '3275498', 'dorna', 'babadi', 'female', 350, '3042509', 'Araghi Street'),
('c6', 'far@gmail.com', '029415t9', 'parsa', 'farin', 'male', 455, '0394502', 'SaadatAbad'),
('c7', 'parham@gmail.com', '32oiwfj', 'parham', 'rahimi', 'male', 234, '94257201', 'Resalat'),
('c8', 'shah@gmail.com', '984579423', 'shahriyar', 'shahbazi', 'male', 233, '32423983', 'Dolat'),
('c9', 'zahraynp@gmail.com', '32094809', 'zahra', 'yoonespour', 'female', 324, '90242342', 'Moein');

-- --------------------------------------------------------

--
-- Table structure for table `Manager`
--

CREATE TABLE `Manager` (
  `ID` varchar(10) NOT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `Password` varchar(10) DEFAULT NULL,
  `FirstName` varchar(10) DEFAULT NULL,
  `LastName` varchar(10) DEFAULT NULL,
  `Sex` varchar(10) DEFAULT NULL,
  `Cellphone` varchar(10) DEFAULT NULL,
  `Address` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Manager`
--

INSERT INTO `Manager` (`ID`, `Email`, `Password`, `FirstName`, `LastName`, `Sex`, `Cellphone`, `Address`) VALUES
('m1', 'asdflkj@gmail.com', 'oqiwr132', 'manager', 'manage1', 'male', '489452', 'Shariati street'),
('m2', 'sfdh@gmail.com', '3290404', 'mohammad', 'manage2', 'female', '348439', 'Tajrish'),
('m3', 'fwiuf@gmail.com', 'iwuf239', 'mafdj', 'manager3', 'male', '23899', 'Teatrshahr'),
('m4', 'asdoif@gmail.com', '9023uf934', 'oofdisa', 'manager4', 'female', '2390480234', 'Vliasr'),
('m5', 'fad@gmail.com', 'rowr3', 'sadfjk', 'manager5', 'female', '09328023', 'Teatrshahr');

-- --------------------------------------------------------

--
-- Table structure for table `Order1`
--

CREATE TABLE `Order1` (
  `ID` varchar(10) NOT NULL,
  `VendorID` varchar(10) DEFAULT NULL,
  `CustomerID` varchar(10) DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL,
  `PaymentType` varchar(20) DEFAULT NULL,
  `CreatedAt` date DEFAULT NULL,
  `DeliveryAddress` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Order1`
--

INSERT INTO `Order1` (`ID`, `VendorID`, `CustomerID`, `Status`, `PaymentType`, `CreatedAt`, `DeliveryAddress`) VALUES
('o1', 'v1', 'c1', 'Submitted', 'Cash', '2018-11-16', 'Shariati'),
('o2', 'v1', 'c2', 'On road', 'Bank portal', '2018-11-03', 'AbbasAbad'),
('o3', 'v5', 'c6', 'Delivered', 'Cash', '2018-10-09', 'Shariati Street'),
('o4', 'v4', 'c5', 'Submitted', 'Bank portal', '2018-10-02', 'Valiasr'),
('o5', 'v3', 'c10', 'On road', 'Cash', '2018-09-12', 'Tajrish'),
('o6', 'v3', 'c1', 'Delivered', 'Cart', '2018-09-21', 'Karimkhan'),
('o7', 'v1', 'c1', 'On Road', 'Cash', '2018-11-03', 'Shariati');

-- --------------------------------------------------------

--
-- Table structure for table `OrderProduct`
--

CREATE TABLE `OrderProduct` (
  `OrderID` varchar(10) NOT NULL,
  `ProductID` varchar(10) NOT NULL,
  `Amount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `OrderProduct`
--

INSERT INTO `OrderProduct` (`OrderID`, `ProductID`, `Amount`) VALUES
('o1', 'p1', 1),
('o1', 'p2', 2),
('o2', 'p3', 3),
('o2', 'p5', 4),
('o2', 'p7', 5),
('o2', 'p8', 6),
('o3', 'p3', 2),
('o3', 'p6', 2),
('o4', 'p1', 1),
('o4', 'p2', 2),
('o4', 'p4', 2),
('o5', 'p2', 2),
('o5', 'p8', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Product`
--

CREATE TABLE `Product` (
  `ID` varchar(10) NOT NULL,
  `Title` varchar(10) DEFAULT NULL,
  `Price` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Product`
--

INSERT INTO `Product` (`ID`, `Title`, `Price`) VALUES
('p1', 'product1', 200),
('p2', 'product2', 300),
('p3', 'product3', 250),
('p4', 'product4', 230),
('p5', 'product5', 270),
('p6', 'product6', 400),
('p7', 'product7', 380),
('p8', 'product8', 550);

-- --------------------------------------------------------

--
-- Table structure for table `Vendor`
--

CREATE TABLE `Vendor` (
  `ID` varchar(10) NOT NULL,
  `Title` varchar(10) DEFAULT NULL,
  `City` varchar(10) DEFAULT NULL,
  `ManagerID` varchar(10) DEFAULT NULL,
  `Phone` varchar(10) DEFAULT NULL,
  `Address` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Vendor`
--

INSERT INTO `Vendor` (`ID`, `Title`, `City`, `ManagerID`, `Phone`, `Address`) VALUES
('v1', 'vendor1', 'Tehran', 'm1', '9123848', 'SaadatAbad'),
('v2', 'vendor2', 'Esfahan', 'm2', '348924', 'CharBagh'),
('v3', 'vendor3', 'Tehran', 'm3', '32849124', 'Karimkhan'),
('v4', 'vendor4', 'Esfahan', 'm4', '39211239', 'DarvazeShiraz'),
('v5', 'vendor5', 'Tehran', 'm5', '4189013', 'Valiasr');

-- --------------------------------------------------------

--
-- Table structure for table `VendorProduct`
--

CREATE TABLE `VendorProduct` (
  `VendorID` varchar(10) NOT NULL,
  `ProductID` varchar(10) NOT NULL,
  `Amount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `VendorProduct`
--

INSERT INTO `VendorProduct` (`VendorID`, `ProductID`, `Amount`) VALUES
('v1', 'p1', 2),
('v1', 'p2', 10),
('v1', 'p4', 10),
('v1', 'p7', 200),
('v2', 'p1', 3),
('v2', 'p3', 3),
('v2', 'p5', 30),
('v2', 'p6', 9),
('v2', 'p8', 220);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Customer`
--
ALTER TABLE `Customer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Manager`
--
ALTER TABLE `Manager`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Order1`
--
ALTER TABLE `Order1`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CustomerID` (`CustomerID`),
  ADD KEY `VendorID` (`VendorID`);

--
-- Indexes for table `OrderProduct`
--
ALTER TABLE `OrderProduct`
  ADD PRIMARY KEY (`OrderID`,`ProductID`),
  ADD KEY `ProductID` (`ProductID`);

--
-- Indexes for table `Product`
--
ALTER TABLE `Product`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Vendor`
--
ALTER TABLE `Vendor`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ManagerID` (`ManagerID`);

--
-- Indexes for table `VendorProduct`
--
ALTER TABLE `VendorProduct`
  ADD PRIMARY KEY (`VendorID`,`ProductID`),
  ADD KEY `ProductID` (`ProductID`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Order1`
--
ALTER TABLE `Order1`
  ADD CONSTRAINT `Order1_ibfk_1` FOREIGN KEY (`CustomerID`) REFERENCES `Customer` (`ID`),
  ADD CONSTRAINT `Order1_ibfk_2` FOREIGN KEY (`VendorID`) REFERENCES `Vendor` (`ID`);

--
-- Constraints for table `OrderProduct`
--
ALTER TABLE `OrderProduct`
  ADD CONSTRAINT `OrderProduct_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `Order1` (`ID`),
  ADD CONSTRAINT `OrderProduct_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`);

--
-- Constraints for table `Vendor`
--
ALTER TABLE `Vendor`
  ADD CONSTRAINT `Vendor_ibfk_1` FOREIGN KEY (`ManagerID`) REFERENCES `Manager` (`ID`);

--
-- Constraints for table `VendorProduct`
--
ALTER TABLE `VendorProduct`
  ADD CONSTRAINT `VendorProduct_ibfk_1` FOREIGN KEY (`VendorID`) REFERENCES `Vendor` (`ID`),
  ADD CONSTRAINT `VendorProduct_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
