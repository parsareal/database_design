-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 20, 2019 at 05:41 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `FinalProject`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddProductNewCustomer` (IN `customerID` INT(10), IN `productID` INT(10), IN `shop` INT(10), IN `orderID` INT(10), IN `amount` INT(5))  BEGIN
   
 	DECLARE orderState varchar(10);
    DECLARE payType varchar(10);
    
    
    SELECT state, paymentType INTO orderState, payType
    FROM Orders
    WHERE ID = orderID and customer = customerID;
    
    
    
    IF orderState = "commited" and payType = "bank" THEN
        
        INSERT INTO Order_Product(productID, orderID, amount) VALUES(productID, orderID, amount);
            
     END IF;
    
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AddProductToOrder` (IN `customerID` INT(10), IN `productID` INT(10), IN `shop` INT(10), IN `orderID` INT(10), IN `amount` INT(5), IN `customerPassword` VARCHAR(255))  BEGIN
     DECLARE orderState varchar(10);
     DECLARE payType varchar(10);
     DECLARE productPrice int;
     DECLARE customerCredit int;
     DECLARE customerPass varchar(255);
     
   	SELECT password INTO customerPass
    FROM Customer
    WHERE ID = customerID;
    
    SELECT state, paymentType INTO orderState, payType
    FROM Orders
    WHERE ID = orderID and customer = customerID;
    
    SELECT price * (1 - offPercent/100) INTO productPrice
    FROM Product
    WHERE ID = productID;
    
    IF MD5(customerPassword) = customerPass THEN
    
    IF orderState = "commited" and payType = "credit" THEN
        SELECT credit INTO customerCredit
        FROM Customer
        WHERE ID = customerID;
        
      IF customerCredit >= productPrice THEN
            INSERT INTO Order_Product(productID, orderID, amount) VALUES(productID, orderID, amount);
            UPDATE Customer
            SET credit = credit - (productPrice * amount)
            WHERE ID = customerID;
      END IF;
     END IF;
    END IF;
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CheckOrder` (IN `orderID` INT(10), IN `missionID` INT(10))  BEGIN
   
   DECLARE initialProductsCount double;
   DECLARE acceptedProductsCount double;
   DECLARE OrderShopID int;
   DECLARE OrderCustomerID int;
   DECLARE supportID int;
   DECLARE readyDeliveries double;
   DECLARE chosenDelivery int;
   DECLARE deliveryCredit float;
   DECLARE orderState varchar(10);
   
   SELECT state INTO orderState
   FROM Orders
   WHERE ID = orderID;
   
   SELECT shop INTO OrderShopID
   FROM Orders
   WHERE ID = orderID;
   
   SELECT COUNT(*) INTO initialProductsCount
   FROM Order_Product as OP
   WHERE OP.orderID = orderID;
   
   
      				#  ********* CHECK ORDER AMOUNTS AMONG SHOP AMOUNTS
                    
   SELECT COUNT(*) INTO acceptedProductsCount
   FROM Product as P, (SELECT orderID, productID, SUM(amount) as newAmount
                       FROM Order_Product as OP
                       WHERE OP.orderID = orderID
                       GROUP BY productID) as T
   WHERE T.orderID = orderID and P.shop = orderShopID and T.productID = P.ID and T.newAmount <= P.amount;
   
   IF orderState = "confirmed" THEN
   
   IF acceptedProductsCount = initialProductsCount THEN
   
      				#  ********* CHECK IF FREE DELIVERY_STAFF EXIST
     
       SELECT COUNT(*) INTO readyDeliveries
       FROM Delivery_Staff
       WHERE shop = orderShopID and state = "ready";
       
       IF readyDeliveries > 0 THEN
       
                    # ****** SELECT A FREE DELIVERY_STAFF *******            
           	SELECT ID INTO chosenDelivery
       		FROM Delivery_Staff
      		WHERE shop = orderShopID and state = "ready"
            LIMIT 1;
            
            INSERT INTO Order_Delivery(orderID, deliveryID) VALUES(orderID, chosenDelivery);
          	
            
                              # ****** UPDATE DELIVERY_STAFF STATE *******
         
            UPDATE Delivery_Staff
            SET state = "delivering"
            WHERE ID = chosenDelivery;
            
                                # ****** UPDATE ORDER STATE *******
                                
            UPDATE Orders
            SET state = "delivering"
            WHERE ID = orderID;
            
            
                                # ****** UPDATE SHOP AMOUNTS *******
                                
                                
             UPDATE Product as PR
            INNER JOIN (SELECT P.ID, (P.amount - T.newAmount) as finalAmount
                               FROM Product as P, (SELECT orderID, productID, SUM(amount) as newAmount
                                                   FROM Order_Product as OP
                                                   WHERE OP.orderID = orderID
                                                   GROUP BY productID) as T
                               WHERE T.orderID = orderID and P.shop = orderShopID and T.productID = P.ID and T.newAmount <= P.amount) as S ON PR.ID = S.ID
            SET PR.amount = S.finalAmount
            WHERE PR.shop = orderShopID;
                               
                               
                                    # ****** UPDATE DELIVERY_STAFF CREDIT *******
            
            SELECT SUM(Product.price * (1 - Product.offPercent/100) * T.newAmount * (5/100)) INTO deliveryCredit
            FROM Product, (SELECT orderID, productID, SUM(amount) as newAmount
                       FROM Order_Product as OP
                       WHERE OP.orderID = orderID
                       GROUP BY productID) as T
            WHERE Product.ID = T.productID;
            
            UPDATE Delivery_Staff
            SET credit = deliveryCredit
            WHERE ID = chosenDelivery;
                     
           	
       ELSEIF readyDeliveries = 0 THEN 
       		
       		UPDATE Orders
            SET state = "rejected"
            WHERE ID = orderID;
            
            SELECT Orders.customer INTO OrderCustomerID
            FROM Orders
            WHERE Orders.ID = orderID;
            
            SELECT ID INTO supportID
            FROM Support_Staff
            WHERE Support_Staff.Shop = OrderShopID
            LIMIT 1;
            
           	INSERT INTO Support_Customer(customerID, supportID, message) VALUES(OrderCustomerID, supportID, "SORRY! THERE IS NOT ANY FREE DELIVERY_STAFF!");
       
       END IF;
        
        
   ELSEIF acceptedProductsCount <= initialProductsCount THEN
   			SELECT ID INTO supportID
            FROM Support_Staff
            WHERE Support_Staff.Shop = OrderShopID
            LIMIT 1;
            
            SELECT Orders.customer INTO OrderCustomerID
            FROM Orders
            WHERE Orders.ID = orderID;
            
           	INSERT INTO Support_Customer(customerID, supportID, message) VALUES(OrderCustomerID, supportID, "SORRY! THERE IS NOT ENOUGH CREDIT IN YOUR ACCOUNT!");
   		
   		UPDATE Orders
        SET state = "rejected"
        WHERE ID = orderID;
   END IF;
   
   END IF;
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConfirmOrder` (IN `customerID` INT(10), IN `orderID` INT(10), IN `customerPassword` VARCHAR(255))  BEGIN	
       DECLARE customerPass varchar(255);
       DECLARE ordID int;
   
       SELECT password INTO customerPass
       FROM Customer
       WHERE ID = customerID;
       
       SELECT ID INTO ordID
       FROM Orders
       WHERE customer = customerID and ID = orderID;
      
      
       IF MD5(customerPassword) = customerPass THEN      
            
            UPDATE Orders as O
        	SET state = "confirmed"
        	WHERE O.ID = orderID;
            
       END IF;
       
	
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConfirmOrderNewCustomer` (IN `customerID` INT(10), IN `orderID` INT(10))  BEGIN
   
       DECLARE ordID int;
   
       
       SELECT ID INTO ordID
       FROM Orders
       WHERE customer = customerID and ID = orderID;
      
      
            
       UPDATE Orders as O
	   SET state = "confirmed"
       WHERE O.ID = ordID;
            

   
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `DeliverOrder` (IN `deliveryID` INT(10), IN `orderID` INT(10))  BEGIN
   
     DECLARE isTrue double;
    
    SELECT COUNT(*) INTO isTrue
    FROM Order_Delivery as OD
    WHERE OD.orderID = orderID and OD.deliveryID = deliveryID;
    
    IF isTrue = 1 THEN
    
        UPDATE Orders as O
        SET state = "delivered"
        WHERE O.ID = orderID;
    
    END IF;
   
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `IncreaseCredit` (IN `ID1` INT(10), IN `credit1` FLOAT, IN `customerPassword` VARCHAR(255))  BEGIN
   DECLARE customerPass varchar(255);
   
   SELECT password INTO customerPass
   FROM Customer
   WHERE ID = ID1;
	
    IF MD5(customerPassword) = customerPass THEN
        UPDATE Customer
        SET credit = credit1
        WHERE ID = ID1;
    END IF;
   
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertAddress` (IN `address` VARCHAR(50), IN `customer` INT(10), IN `customerPassword` VARCHAR(255))  BEGIN
	DECLARE customerPass varchar(255);
    
    SELECT password into customerPass
    FROM Customer
    WHERE ID = customerID;
    
    SELECT customerPass;
    SELECT MD5(customerPassword);
    
    IF MD5(customerPassword) = customerPass THEN
    	INSERT INTO Address(address, customer)
  		VALUES(address, customerID);
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertCustomer` (IN `username` VARCHAR(10), IN `firstName` VARCHAR(10), IN `lastName` VARCHAR(15), IN `password` VARCHAR(20), IN `email` VARCHAR(20), IN `zipcode` INT(15), IN `gender` VARCHAR(10))  BEGIN
   INSERT INTO Customer(username, firstName, lastName, password, email, zipcode, gender, credit)
   VALUES(username, firstName, lastName, MD5(password), email, zipcode, gender, 0);
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertDeliveryStaff` (IN `firstName` VARCHAR(10), IN `lastName` VARCHAR(15), IN `phone` VARCHAR(10), IN `Shop` INT(10))  BEGIN
	INSERT INTO Delivery_Staff(firstName, lastName, phone, state, credit, Shop)
   VALUES(firstName, lastName, phone, "ready", 0, Shop);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertMissionStaff` (IN `Shop` VARCHAR(10), IN `firstName` VARCHAR(10), IN `lastName` VARCHAR(15))  BEGIN
  INSERT INTO Mission_Staff(Shop, firstName, lastName)
   VALUES(Shop, firstName, lastName);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertPhone` (IN `phoneNumber` VARCHAR(10), IN `customerID` INT(10), IN `customerPassword` VARCHAR(255))  BEGIN
	DECLARE customerPass varchar(255);
    
    SELECT password into customerPass
    FROM Customer
    WHERE ID = customerID;
    
    SELECT customerPass;
    SELECT MD5(customerPassword);
    
    IF MD5(customerPassword) = customerPass THEN
    	INSERT INTO Phone(phoneNumber, customer)
  		VALUES(phoneNumber, customerID);
    END IF;
  
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertProduct` (IN `Shop` VARCHAR(10), IN `title` VARCHAR(10), IN `price` FLOAT, IN `offPercent` FLOAT, IN `amount` INT(5))  BEGIN
  INSERT INTO Product(ID, Shop, title, price, offPercent, amount)
   VALUES(ID, Shop, title, price, offPercent, amount);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertShop` (IN `title` VARCHAR(20), IN `city` VARCHAR(20), IN `address` VARCHAR(50), IN `phone` VARCHAR(10), IN `openTime` TIME, IN `closeTime` TIME)  BEGIN
   INSERT INTO Shop(title, city, address, phone, openTime, closeTime)
   VALUES(title, city, address, phone, openTime, closeTime);
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertSupportStaff` (IN `Shop` INT(10), IN `firstName` VARCHAR(10), IN `lastName` VARCHAR(15), IN `phone` VARCHAR(10), IN `address` VARCHAR(50))  BEGIN
  INSERT INTO Support_Staff(Shop, firstName, lastName, phone, address)
   VALUES(Shop, firstName, lastName, phone, address);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Q1` (IN `shopID` INT(10))  BEGIN

    SELECT *
    FROM (SELECT P.ID, P.title, P.shop, P.price, SUM(OP.amount) as sumAmount
        FROM Product as P, Order_Product as OP
        WHERE P.ID = OP.productID AND P.shop = shopID
        GROUP BY P.ID) as T
    ORDER BY T.shop ASC, T.sumAmount DESC
    LIMIT 5;
    
   
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Q2` ()  BEGIN
   	SELECT *
    FROM Phone
    WHERE Phone.customer IN (SELECT Orders.customer
                               FROM Orders
                               WHERE Orders.state = "rejected");
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Q3` ()  BEGIN
  		DECLARE avrCustomer float;
        DECLARE avrNewCustomer float;
         
        SELECT AVG(Product.price * (1 - Product.offPercent/100) * Order_Product.amount) INTO avrNewCustomer
        FROM Product, Order_Product, Orders, Customer
        WHERE Product.ID = Order_Product.productID AND Order_Product.orderID = Orders.ID AND Orders.customer = Customer.ID AND Customer.username = "guest";
        
        SELECT AVG(Product.price * (1 - Product.offPercent/100) * Order_Product.amount) INTO avrCustomer
        FROM Product, Order_Product, Orders, Customer
        WHERE Product.ID = Order_Product.productID AND Order_Product.orderID = Orders.ID AND Orders.customer = Customer.ID AND Customer.username != "guest";
   		
        SELECT avrCustomer;
        SELECT avrNewCustomer;
        SELECT (avrCustomer - avrNewCustomer) as Difference;
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Q4` ()  BEGIN
   		
   
        
        SELECT *, MAX(avrOrders)
        FROM Delivery_Staff, (SELECT Order_Delivery.deliveryID, AVG(Product.price * (1 - Product.offPercent/100) * Order_Product.amount) as avrOrders
        FROM Product, Order_Product, Orders, Customer, Order_Delivery
        WHERE Product.ID = Order_Product.productID AND Order_Product.orderID = Orders.ID AND Order_Delivery.orderID = Orders.ID AND Orders.customer = Customer.ID AND Customer.username = "guest"
        GROUP BY Order_Delivery.deliveryID) as T
        WHERE Delivery_Staff.ID = T.deliveryID;
        
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Q5` ()  BEGIN
      DECLARE maxTime double;
      
      SELECT MAX(Shop.closeTime - Shop.openTime) INTO maxTime
      FROM Shop;
  	 	
      SELECT *
      FROM Shop
      WHERE (Shop.closeTime - Shop.openTime) = maxTime;
      
   END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SetOrder` (IN `customerID` INT(10), IN `productID` INT(10), IN `shop` INT(10), IN `paymentType` VARCHAR(10), IN `address` VARCHAR(50), IN `customerPassword` VARCHAR(255), IN `amount` INT(5))  BEGIN
 
    DECLARE customerCredit int;
    DECLARE productPrice int;
    DECLARE shopOpenTime time;
    DECLARE shopCloseTime time;
    DECLARE percent double;
   	DECLARE customerPass varchar(255);
    
    SELECT password INTO customerPass
    FROM Customer
    WHERE ID = customerID;
    
    SELECT price, 1 - offPercent/100 INTO productPrice, percent
    FROM Product
    WHERE ID = productID;
    
    SELECT openTime INTO shopOpenTime
    FROM Shop
    WHERE ID = shop;
    
    
    SELECT closeTime INTO shopCloseTime
    FROM Shop
    WHERE ID = shop;
    
    SELECT CURRENT_TIME;
    SELECT percent;
    SELECT amount;
    
    			# CHECK CUSTOMER PASSWORD
                
    IF MD5(customerPassword) = customerPass THEN
    
    			# CHECK CURRENT TIME AND OPEN AND CLOSE TIME
                
    IF CURRENT_TIME > shopOpenTime and CURRENT_TIME < shopCloseTime THEN
    	IF paymentType = "credit" THEN
        SELECT credit INTO customerCredit
        FROM Customer
        WHERE ID = customerID;
        
        
    			# CHECK PRODUCT PRICE AND CUSTOMER CREDIT
      
      IF customerCredit >= (productPrice*amount*percent) THEN
      
    			# INSERT ORDER TUPPLE IN ORDERS
      
        INSERT INTO Orders(shop, customer, state, paymentType, address, orderDate) VALUES(shop, customerID, "commited", paymentType, address, CURRENT_DATE);
        
        INSERT INTO Order_Product(productID, orderID, amount) VALUES(productID, LAST_INSERT_ID(), amount);
        
    			# UPDATE CUSTOMER CREDIT
        
  		UPDATE Customer
        SET credit = credit - (productPrice * amount * percent)
        WHERE ID = customerID;
           
       END IF;
        
    ELSEIF paymentType = "bank" THEN
      INSERT INTO Orders(shop, customer, state, paymentType, address, orderDate) VALUES(shop, customerID, "commited", paymentType, address, CURRENT_DATE);
      INSERT INTO Order_Product(productID, orderID, amount) VALUES(productID, LAST_INSERT_ID(), amount);
        
        
    END IF;
    
    
    END IF;
    
    END IF;
    
 END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SetOrderNewCustomer` (IN `productID` INT(10), IN `shop` INT(10), IN `amount` INT(5), IN `paymentType` VARCHAR(10), IN `address` VARCHAR(50), IN `firstName` VARCHAR(10), IN `lastName` VARCHAR(15))  BEGIN
   
    DECLARE productPrice int;
    DECLARE shopOpenTime time;
    DECLARE shopCloseTime time;
    DECLARE percent double;
    
    INSERT INTO Customer(username, firstName, lastName, password, email, 		zipcode, gender, credit)
   VALUES("guest", firstName, lastName, null, null, null, null, null);
    
    SELECT price, 1 - offPercent/100 INTO productPrice, percent
    FROM Product
    WHERE ID = productID;
    
    SELECT openTime INTO shopOpenTime
    FROM Shop
    WHERE ID = shop;
    
    
    SELECT closeTime INTO shopCloseTime
    FROM Shop
    WHERE ID = shop;
    
    SELECT CURRENT_TIME;
    SELECT percent;
    SELECT amount;
    
                
    
    			# CHECK CURRENT TIME AND OPEN AND CLOSE TIME
                
    IF CURRENT_TIME > shopOpenTime and CURRENT_TIME < shopCloseTime THEN
        
    IF paymentType = "bank" THEN
      INSERT INTO Orders(shop, customer, state, paymentType, address, orderDate) VALUES(shop, LAST_INSERT_ID(), "commited", paymentType, address, CURRENT_DATE);
      INSERT INTO Order_Product(productID, orderID, amount) VALUES(productID, LAST_INSERT_ID(), amount);
        
        
    END IF;
    
    
    END IF;
    
    
    
   END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Address`
--

CREATE TABLE `Address` (
  `address` varchar(50) NOT NULL,
  `customer` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Customer`
--

CREATE TABLE `Customer` (
  `ID` int(10) NOT NULL,
  `username` varchar(10) DEFAULT NULL,
  `firstName` varchar(10) DEFAULT NULL,
  `lastName` varchar(15) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `zipcode` int(15) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `credit` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Customer`
--

INSERT INTO `Customer` (`ID`, `username`, `firstName`, `lastName`, `password`, `email`, `zipcode`, `gender`, `credit`) VALUES
(11, 'boojy', 'marcelo', 'navas', '7bd0d5cc82d620c0ad4289f580a20efc', 'parsa@gmail.com', 213241438, 'male', 53),
(12, 'karmyc', 'lionel', 'messi', '7bd0d5cc82d620c0ad4289f580a20efc', 'parsa@gmail.com', 21324138, 'male', 3934),
(13, 'malena', 'moonica', 'belouchi', '7bd0d5cc82d620c0ad4289f580a20efc', 'parsa@gmail.com', 21324138, 'female', 2230),
(14, 'lash', 'karim', 'benzema', '7bd0d5cc82d620c0ad4289f580a20efc', 'parsa@gmail.com', 21324138, 'male', 3702),
(16, 'guest', 'bayram', 'loader', NULL, NULL, NULL, NULL, NULL),
(17, 'guest', 'dodo', 'amali', NULL, NULL, NULL, NULL, NULL);

--
-- Triggers `Customer`
--
DELIMITER $$
CREATE TRIGGER `customer_signup` AFTER INSERT ON `Customer` FOR EACH ROW BEGIN
    INSERT INTO LOGS_SignUp
    SET title = 'SIGNUP_NEW_CUSTOMER',
    customerID = NEW.ID,
    lastname = NEW.lastname,
    signupDate = NOW(); 
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `customer_update_credit` BEFORE UPDATE ON `Customer` FOR EACH ROW BEGIN
    INSERT INTO LOGS_IncreaseCredit
    SET title = "UPDATE_CREDIT",
    customerID = OLD.ID,
    lastname = OLD.lastname,
    credit = NEW.credit,
    updateDate = NOW(); 
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Delivery_Staff`
--

CREATE TABLE `Delivery_Staff` (
  `ID` int(10) NOT NULL,
  `Shop` int(10) NOT NULL,
  `firstName` varchar(10) DEFAULT NULL,
  `lastName` varchar(15) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `state` varchar(15) DEFAULT NULL,
  `credit` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Delivery_Staff`
--

INSERT INTO `Delivery_Staff` (`ID`, `Shop`, `firstName`, `lastName`, `phone`, `state`, `credit`) VALUES
(6, 2, 'maryam', 'abbasi', '091238234', 'delivering', 4.5),
(7, 2, 'karim', 'benzema', '091238234', 'delivering', 2.85),
(9, 2, 'cris', 'ronaldo', '091238234', 'delivering', 8.5),
(10, 2, 'luka', 'modrich', '091238234', 'delivering', 2.4),
(11, 3, 'luis', 'suarez', '98342543', 'ready', 9.8),
(12, 3, 'sergio', 'buskets', '98342543', 'ready', 14.795),
(13, 3, 'nelson', 'semedo', '98342543', 'ready', 0),
(14, 3, 'hasan', 'abdolkarim', '98342543', 'ready', 0);

--
-- Triggers `Delivery_Staff`
--
DELIMITER $$
CREATE TRIGGER `deliveryUpdate` AFTER UPDATE ON `Delivery_Staff` FOR EACH ROW BEGIN
    INSERT INTO LOGS_UpdateDelivery
    SET title = 'UPDATE_DELIVERY_ACCOUNT',
    deliveryID = NEW.ID,
    lastname = NEW.lastname,
    oldState = OLD.state,
    newState = NEW.state,
    oldCredit = OLD.credit,
    newCredit = NEW.credit,
    updateDate = NOW(); 
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `LOGS_IncreaseCredit`
--

CREATE TABLE `LOGS_IncreaseCredit` (
  `ID` int(10) NOT NULL,
  `title` varchar(30) DEFAULT NULL,
  `updateDate` date DEFAULT NULL,
  `credit` float DEFAULT NULL,
  `customerID` int(10) DEFAULT NULL,
  `lastName` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `LOGS_IncreaseCredit`
--

INSERT INTO `LOGS_IncreaseCredit` (`ID`, `title`, `updateDate`, `credit`, `customerID`, `lastName`) VALUES
(1, 'UPDATE_CREDIT', '2019-01-17', 1000, 11, 'navas'),
(2, 'UPDATE_CREDIT', '2019-01-17', 910, 11, 'navas'),
(3, 'UPDATE_CREDIT', '2019-01-18', 853, 11, 'navas'),
(4, 'UPDATE_CREDIT', '2019-01-18', 796, 11, 'navas'),
(5, 'UPDATE_CREDIT', '2019-01-18', 626, 11, 'navas'),
(6, 'UPDATE_CREDIT', '2019-01-18', 578, 11, 'navas'),
(7, 'UPDATE_CREDIT', '2019-01-18', 53, 11, 'navas'),
(8, 'UPDATE_CREDIT', '2019-01-18', 2400, 13, 'belouchi'),
(9, 'UPDATE_CREDIT', '2019-01-18', 2230, 13, 'belouchi'),
(10, 'UPDATE_CREDIT', '2019-01-18', 5000, 12, 'messi'),
(11, 'UPDATE_CREDIT', '2019-01-18', 4000, 14, 'benzema'),
(12, 'UPDATE_CREDIT', '2019-01-18', 4840, 12, 'messi'),
(13, 'UPDATE_CREDIT', '2019-01-18', 3744, 14, 'benzema'),
(14, 'UPDATE_CREDIT', '2019-01-18', 4804, 12, 'messi'),
(15, 'UPDATE_CREDIT', '2019-01-18', 3702, 14, 'benzema'),
(16, 'UPDATE_CREDIT', '2019-01-19', 4549, 12, 'messi'),
(17, 'UPDATE_CREDIT', '2019-01-20', 3982, 12, 'messi'),
(18, 'UPDATE_CREDIT', '2019-01-20', 3934, 12, 'messi');

-- --------------------------------------------------------

--
-- Table structure for table `LOGS_SignUp`
--

CREATE TABLE `LOGS_SignUp` (
  `ID` int(10) NOT NULL,
  `title` varchar(30) DEFAULT NULL,
  `signupDate` date DEFAULT NULL,
  `customerID` int(10) DEFAULT NULL,
  `lastName` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `LOGS_SignUp`
--

INSERT INTO `LOGS_SignUp` (`ID`, `title`, `signupDate`, `customerID`, `lastName`) VALUES
(1, 'SIGNUP_NEW_CUSTOMER', '2019-01-17', 0, 'belouchi'),
(2, 'SIGNUP_NEW_CUSTOMER', '2019-01-17', 14, 'benzema'),
(3, 'SIGNUP_NEW_CUSTOMER', '2019-01-18', 15, 'guest'),
(4, 'SIGNUP_NEW_CUSTOMER', '2019-01-18', 16, 'loader'),
(5, 'SIGNUP_NEW_CUSTOMER', '2019-01-18', 17, 'amali');

-- --------------------------------------------------------

--
-- Table structure for table `LOGS_UpdateDelivery`
--

CREATE TABLE `LOGS_UpdateDelivery` (
  `ID` int(10) NOT NULL,
  `title` varchar(30) DEFAULT NULL,
  `updateDate` date DEFAULT NULL,
  `oldState` varchar(10) DEFAULT NULL,
  `newState` varchar(10) NOT NULL,
  `deliveryID` int(10) DEFAULT NULL,
  `oldCredit` float DEFAULT NULL,
  `newCredit` float NOT NULL,
  `lastName` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `LOGS_UpdateDelivery`
--

INSERT INTO `LOGS_UpdateDelivery` (`ID`, `title`, `updateDate`, `oldState`, `newState`, `deliveryID`, `oldCredit`, `newCredit`, `lastName`) VALUES
(1, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-17', 'delivering', '', 6, 0, 0, 'abbasi'),
(2, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-17', 'delivering', '', 6, 4.5, 0, 'abbasi'),
(3, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-17', 'ready', '', 6, 4.5, 0, 'abbasi'),
(4, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'ready', 'delivering', 6, 4.5, 4.5, 'abbasi'),
(8, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'ready', 7, 0, 0, 'benzema'),
(9, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'ready', 9, 0, 0, 'ronaldo'),
(10, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'ready', 10, 0, 0, 'modrich'),
(11, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'ready', 'delivering', 7, 0, 0, 'benzema'),
(12, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'delivering', 7, 0, 2.85, 'benzema'),
(13, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'ready', 'delivering', 9, 0, 0, 'ronaldo'),
(14, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'delivering', 9, 0, 2.85, 'ronaldo'),
(15, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'ready', 9, 2.85, 2.85, 'ronaldo'),
(16, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'ready', 'delivering', 9, 2.85, 2.85, 'ronaldo'),
(17, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'delivering', 9, 2.85, 5.925, 'ronaldo'),
(18, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'ready', 9, 5.925, 5.925, 'ronaldo'),
(19, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'ready', 'delivering', 9, 5.925, 5.925, 'ronaldo'),
(20, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'delivering', 9, 5.925, 8.5, 'ronaldo'),
(21, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'ready', 'delivering', 10, 0, 0, 'modrich'),
(22, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'delivering', 10, 0, 2.4, 'modrich'),
(27, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'ready', 'delivering', 11, 0, 0, 'suarez'),
(28, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'delivering', 11, 0, 9.8, 'suarez'),
(29, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'ready', 'delivering', 12, 0, 0, 'buskets'),
(30, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'delivering', 12, 0, 14.795, 'buskets'),
(31, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'ready', 11, 9.8, 9.8, 'suarez'),
(32, 'UPDATE_DELIVERY_ACCOUNT', '2019-01-18', 'delivering', 'ready', 12, 14.795, 14.795, 'buskets');

-- --------------------------------------------------------

--
-- Table structure for table `LOGS_UpdateOrder`
--

CREATE TABLE `LOGS_UpdateOrder` (
  `ID` int(10) NOT NULL,
  `title` varchar(30) DEFAULT NULL,
  `updateDate` date DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  `orderID` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `LOGS_UpdateOrder`
--

INSERT INTO `LOGS_UpdateOrder` (`ID`, `title`, `updateDate`, `state`, `orderID`) VALUES
(1, 'UPDATE_ORDER_STATE', '2019-01-17', 'delivering', 43),
(2, 'UPDATE_ORDER_STATE', '2019-01-17', 'delivered', 43),
(11, 'UPDATE_ORDER_STATE', '2019-01-18', 'delivering', 44),
(12, 'UPDATE_ORDER_STATE', '2019-01-18', 'delivered', 44),
(13, 'UPDATE_ORDER_STATE', '2019-01-18', 'confirmed', 45),
(14, 'UPDATE_ORDER_STATE', '2019-01-18', 'delivering', 45),
(15, 'UPDATE_ORDER_STATE', '2019-01-18', 'delivered', 45),
(16, 'UPDATE_ORDER_STATE', '2019-01-18', 'rejected', 46),
(17, 'UPDATE_ORDER_STATE', '2019-01-18', 'confirmed', 47),
(18, 'UPDATE_ORDER_STATE', '2019-01-18', 'delivering', 47),
(19, 'UPDATE_ORDER_STATE', '2019-01-18', 'delivered', 47),
(20, 'UPDATE_ORDER_STATE', '2019-01-18', 'confirmed', 48),
(21, 'UPDATE_ORDER_STATE', '2019-01-18', 'confirmed', 49),
(22, 'UPDATE_ORDER_STATE', '2019-01-18', 'confirmed', 50),
(23, 'UPDATE_ORDER_STATE', '2019-01-18', 'delivering', 48),
(24, 'UPDATE_ORDER_STATE', '2019-01-18', 'delivering', 49),
(25, 'UPDATE_ORDER_STATE', '2019-01-18', 'rejected', 50),
(26, 'UPDATE_ORDER_STATE', '2019-01-18', 'confirmed', 51),
(27, 'UPDATE_ORDER_STATE', '2019-01-18', 'rejected', 51),
(28, 'UPDATE_ORDER_STATE', '2019-01-18', 'confirmed', 52),
(29, 'UPDATE_ORDER_STATE', '2019-01-18', 'confirmed', 53),
(30, 'UPDATE_ORDER_STATE', '2019-01-18', 'delivering', 52),
(31, 'UPDATE_ORDER_STATE', '2019-01-18', 'delivering', 53),
(32, 'UPDATE_ORDER_STATE', '2019-01-18', 'delivered', 52),
(33, 'UPDATE_ORDER_STATE', '2019-01-18', 'delivered', 53),
(34, 'UPDATE_ORDER_STATE', '2019-01-19', 'confirmed', 54),
(35, 'UPDATE_ORDER_STATE', '2019-01-19', 'rejected', 54),
(36, 'UPDATE_ORDER_STATE', '2019-01-20', 'confirmed', 55);

-- --------------------------------------------------------

--
-- Table structure for table `Mission_Staff`
--

CREATE TABLE `Mission_Staff` (
  `ID` int(10) NOT NULL,
  `Shop` int(10) NOT NULL,
  `firstName` varchar(10) DEFAULT NULL,
  `lastName` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Mission_Staff`
--

INSERT INTO `Mission_Staff` (`ID`, `Shop`, `firstName`, `lastName`) VALUES
(2, 2, 'karim', 'aliyari'),
(3, 3, 'ivan', 'rakitic');

-- --------------------------------------------------------

--
-- Table structure for table `Orders`
--

CREATE TABLE `Orders` (
  `ID` int(10) NOT NULL,
  `shop` int(10) DEFAULT NULL,
  `customer` int(10) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  `paymentType` varchar(10) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `orderDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Orders`
--

INSERT INTO `Orders` (`ID`, `shop`, `customer`, `state`, `paymentType`, `address`, `orderDate`) VALUES
(43, 2, 11, 'delivered', 'credit', 'shariati', '2019-01-17'),
(44, 2, 11, 'delivered', 'credit', 'shariati', '2019-01-18'),
(45, 2, 11, 'delivered', 'credit', 'shariati', '2019-01-18'),
(46, 2, 16, 'rejected', 'bank', 'seyedkhandan', '2019-01-18'),
(47, 2, 17, 'delivered', 'bank', 'sayadshirazi', '2019-01-18'),
(48, 2, 11, 'delivering', 'credit', 'shariati', '2019-01-18'),
(49, 2, 11, 'delivering', 'credit', 'shariati', '2019-01-18'),
(50, 2, 11, 'rejected', 'credit', 'shariati', '2019-01-18'),
(51, 2, 13, 'rejected', 'credit', 'shariati', '2019-01-18'),
(52, 3, 12, 'delivered', 'credit', 'shariati', '2019-01-18'),
(53, 3, 14, 'delivered', 'credit', 'shariati', '2019-01-18'),
(54, 2, 12, 'rejected', 'credit', 'shariati', '2019-01-19'),
(55, 2, 12, 'confirmed', 'credit', 'ferdos', '2019-01-20'),
(56, 2, 12, 'commited', 'credit', 'lombard', '2019-01-20');

--
-- Triggers `Orders`
--
DELIMITER $$
CREATE TRIGGER `order_update_state` AFTER UPDATE ON `Orders` FOR EACH ROW BEGIN
  IF NEW.state = "delivered" THEN
      UPDATE Delivery_Staff as DS
        SET state = "ready"
        WHERE DS.ID = (SELECT deliveryID
                      FROM Order_Delivery
                      WHERE orderID = OLD.ID);
    END IF;
    
  
    INSERT INTO LOGS_UpdateOrder
    SET title = 'UPDATE_ORDER_STATE',
    orderID = OLD.ID,
    state = NEW.state,
    updateDate = NOW();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Order_Delivery`
--

CREATE TABLE `Order_Delivery` (
  `orderID` int(10) NOT NULL,
  `deliveryID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Order_Delivery`
--

INSERT INTO `Order_Delivery` (`orderID`, `deliveryID`) VALUES
(43, 6),
(44, 6),
(44, 7),
(45, 9),
(47, 9),
(48, 9),
(49, 10),
(52, 11),
(53, 12);

-- --------------------------------------------------------

--
-- Table structure for table `Order_Product`
--

CREATE TABLE `Order_Product` (
  `productID` int(10) NOT NULL,
  `orderID` int(10) NOT NULL,
  `amount` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Order_Product`
--

INSERT INTO `Order_Product` (`productID`, `orderID`, `amount`) VALUES
(6, 43, 1),
(6, 47, 1),
(8, 44, 2),
(8, 45, 2),
(8, 46, 1),
(8, 47, 1),
(9, 55, 3),
(10, 49, 1),
(10, 56, 1),
(11, 48, 2),
(11, 51, 2),
(11, 54, 3),
(12, 50, 1),
(13, 53, 6),
(16, 52, 2),
(17, 53, 4),
(19, 52, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Phone`
--

CREATE TABLE `Phone` (
  `phoneNumber` varchar(10) NOT NULL,
  `customer` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Phone`
--

INSERT INTO `Phone` (`phoneNumber`, `customer`) VALUES
('22324834', 13),
('23582353', 11),
('3453948', 14),
('8934245', 12),
('93845983', 14),
('98345348', 11);

-- --------------------------------------------------------

--
-- Table structure for table `Product`
--

CREATE TABLE `Product` (
  `ID` int(10) NOT NULL,
  `shop` int(10) NOT NULL,
  `title` varchar(10) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `offPercent` float DEFAULT NULL,
  `amount` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Product`
--

INSERT INTO `Product` (`ID`, `shop`, `title`, `price`, `offPercent`, `amount`) VALUES
(6, 2, 'shirt', 100, 10, 198),
(7, 2, 'coat', 150, 10, 150),
(8, 2, 'tie', 30, 5, 295),
(9, 2, 'jeans', 210, 10, 60),
(10, 2, 'T_shirt', 60, 20, 2999),
(11, 2, 'sweatshirt', 100, 15, 248),
(12, 2, 'suit', 750, 30, 100),
(13, 3, 'shampoo', 7, 5, 494),
(14, 3, 'kit-kat', 10, 10, 400),
(15, 3, 'cleaner', 60, 15, 100),
(16, 3, 'book', 20, 10, 48),
(17, 3, 'dish', 80, 20, 66),
(18, 3, 'doll', 25, 5, 100),
(19, 3, 'tennis roc', 200, 20, 49);

-- --------------------------------------------------------

--
-- Table structure for table `Shop`
--

CREATE TABLE `Shop` (
  `ID` int(10) NOT NULL,
  `title` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `openTime` time NOT NULL,
  `closeTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Shop`
--

INSERT INTO `Shop` (`ID`, `title`, `city`, `address`, `phone`, `openTime`, `closeTime`) VALUES
(2, 'hakoopian', 'Tehran', 'abbas abad', '22824577', '08:00:00', '23:00:00'),
(3, 'casco', 'Toronto', '123 street', '93824577', '06:00:00', '23:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `Support_Customer`
--

CREATE TABLE `Support_Customer` (
  `customerID` int(10) NOT NULL,
  `supportID` int(10) NOT NULL,
  `message` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Support_Customer`
--

INSERT INTO `Support_Customer` (`customerID`, `supportID`, `message`) VALUES
(12, 1, 'SORRY! THERE IS NOT ANY FREE DELIVERY_STAFF!');

-- --------------------------------------------------------

--
-- Table structure for table `Support_Staff`
--

CREATE TABLE `Support_Staff` (
  `ID` int(10) NOT NULL,
  `Shop` int(10) NOT NULL,
  `firstName` varchar(10) DEFAULT NULL,
  `lastName` varchar(15) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Support_Staff`
--

INSERT INTO `Support_Staff` (`ID`, `Shop`, `firstName`, `lastName`, `phone`, `address`) VALUES
(1, 2, 'ali', 'gharebaghi', '0912231843', 'lombard street'),
(2, 2, 'parsa', 'eskandar', '23104893', 'lalezar street'),
(3, 3, 'mohammad', 'smdi', '8745854', 'karimkhan street'),
(4, 3, 'karim', 'chekhe', '93128434', 'karimkhan street');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Address`
--
ALTER TABLE `Address`
  ADD PRIMARY KEY (`address`,`customer`),
  ADD KEY `customer` (`customer`);

--
-- Indexes for table `Customer`
--
ALTER TABLE `Customer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Delivery_Staff`
--
ALTER TABLE `Delivery_Staff`
  ADD PRIMARY KEY (`ID`,`Shop`),
  ADD KEY `Shop` (`Shop`);

--
-- Indexes for table `LOGS_IncreaseCredit`
--
ALTER TABLE `LOGS_IncreaseCredit`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `LOGS_SignUp`
--
ALTER TABLE `LOGS_SignUp`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `LOGS_UpdateDelivery`
--
ALTER TABLE `LOGS_UpdateDelivery`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `LOGS_UpdateOrder`
--
ALTER TABLE `LOGS_UpdateOrder`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Mission_Staff`
--
ALTER TABLE `Mission_Staff`
  ADD PRIMARY KEY (`ID`,`Shop`),
  ADD KEY `Shop` (`Shop`);

--
-- Indexes for table `Orders`
--
ALTER TABLE `Orders`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `shop` (`shop`),
  ADD KEY `customer` (`customer`);

--
-- Indexes for table `Order_Delivery`
--
ALTER TABLE `Order_Delivery`
  ADD PRIMARY KEY (`orderID`,`deliveryID`),
  ADD KEY `deliveryID` (`deliveryID`);

--
-- Indexes for table `Order_Product`
--
ALTER TABLE `Order_Product`
  ADD PRIMARY KEY (`productID`,`orderID`),
  ADD KEY `orderID` (`orderID`);

--
-- Indexes for table `Phone`
--
ALTER TABLE `Phone`
  ADD PRIMARY KEY (`phoneNumber`,`customer`),
  ADD KEY `customer` (`customer`);

--
-- Indexes for table `Product`
--
ALTER TABLE `Product`
  ADD PRIMARY KEY (`ID`,`shop`),
  ADD KEY `shop` (`shop`);

--
-- Indexes for table `Shop`
--
ALTER TABLE `Shop`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Support_Customer`
--
ALTER TABLE `Support_Customer`
  ADD PRIMARY KEY (`customerID`,`supportID`,`message`);

--
-- Indexes for table `Support_Staff`
--
ALTER TABLE `Support_Staff`
  ADD PRIMARY KEY (`ID`,`Shop`),
  ADD KEY `Shop` (`Shop`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Customer`
--
ALTER TABLE `Customer`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `Delivery_Staff`
--
ALTER TABLE `Delivery_Staff`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `LOGS_IncreaseCredit`
--
ALTER TABLE `LOGS_IncreaseCredit`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `LOGS_SignUp`
--
ALTER TABLE `LOGS_SignUp`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `LOGS_UpdateDelivery`
--
ALTER TABLE `LOGS_UpdateDelivery`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `LOGS_UpdateOrder`
--
ALTER TABLE `LOGS_UpdateOrder`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `Mission_Staff`
--
ALTER TABLE `Mission_Staff`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Orders`
--
ALTER TABLE `Orders`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `Product`
--
ALTER TABLE `Product`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `Shop`
--
ALTER TABLE `Shop`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Support_Staff`
--
ALTER TABLE `Support_Staff`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Address`
--
ALTER TABLE `Address`
  ADD CONSTRAINT `Address_ibfk_1` FOREIGN KEY (`customer`) REFERENCES `Customer` (`ID`);

--
-- Constraints for table `Delivery_Staff`
--
ALTER TABLE `Delivery_Staff`
  ADD CONSTRAINT `Delivery_Staff_ibfk_1` FOREIGN KEY (`Shop`) REFERENCES `Shop` (`ID`);

--
-- Constraints for table `Mission_Staff`
--
ALTER TABLE `Mission_Staff`
  ADD CONSTRAINT `Mission_Staff_ibfk_1` FOREIGN KEY (`Shop`) REFERENCES `Shop` (`ID`);

--
-- Constraints for table `Orders`
--
ALTER TABLE `Orders`
  ADD CONSTRAINT `Orders_ibfk_1` FOREIGN KEY (`shop`) REFERENCES `Shop` (`ID`),
  ADD CONSTRAINT `Orders_ibfk_2` FOREIGN KEY (`customer`) REFERENCES `Customer` (`ID`);

--
-- Constraints for table `Order_Delivery`
--
ALTER TABLE `Order_Delivery`
  ADD CONSTRAINT `Order_Delivery_ibfk_1` FOREIGN KEY (`orderID`) REFERENCES `Orders` (`ID`),
  ADD CONSTRAINT `Order_Delivery_ibfk_2` FOREIGN KEY (`deliveryID`) REFERENCES `Delivery_Staff` (`ID`);

--
-- Constraints for table `Order_Product`
--
ALTER TABLE `Order_Product`
  ADD CONSTRAINT `Order_Product_ibfk_1` FOREIGN KEY (`productID`) REFERENCES `Product` (`ID`),
  ADD CONSTRAINT `Order_Product_ibfk_2` FOREIGN KEY (`orderID`) REFERENCES `Orders` (`ID`);

--
-- Constraints for table `Phone`
--
ALTER TABLE `Phone`
  ADD CONSTRAINT `Phone_ibfk_1` FOREIGN KEY (`customer`) REFERENCES `Customer` (`ID`);

--
-- Constraints for table `Product`
--
ALTER TABLE `Product`
  ADD CONSTRAINT `Product_ibfk_1` FOREIGN KEY (`shop`) REFERENCES `Shop` (`ID`);

--
-- Constraints for table `Support_Staff`
--
ALTER TABLE `Support_Staff`
  ADD CONSTRAINT `Support_Staff_ibfk_1` FOREIGN KEY (`Shop`) REFERENCES `Shop` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
